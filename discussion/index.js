console.log("Hello World");

// JS OBjects
// JS Objects is a way to define real world objects with its own characteristics.
// It is also a way to organize data with more context through the use of key-value pairs

/*
	loyal,
	huggable,
	4 legs,
	furry,
	different colors,
	tail,
	breed

*/
// Create a list, use an array
// let doggo = ["loyal",4,"tail","Husky"];

// Describe something with characteristics, use object
let dog = {

	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: true,
	isLoyal: true
}

// Mini Activity

let videoGame = {

	title: "Stray",
	publisher: "Annapurna Interactive",
	year: 2022,
	price: 1690,
	isAvailable: true

}

console.log(videoGame);

// [] are used to create arrays
// {} are used to create objects
// Objects are composed of key-value pairs.
// Key - provides label to the values.
// Each key-value pair together are called properties

// Accessing Array Items = arrName[index]
// Accessing Object Properties = objectName.propertyName

console.log(videoGame.title);
console.log(videoGame.publisher);

videoGame.price = 200;
console.log(videoGame);

videoGame.title = "Final Fantasy X";
videoGame.publisher = "Square Enix";
videoGame.year = 2001;

console.log(videoGame); 

// Objects can also contain objects and arrays

let course = {

	title: "Philosophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr.Johnson", "Mrs.Smith", "Mr.Francis"]
}

console.log(course);
console.log(course.instructors);
console.log(course.instructors[1]);
course.instructors.pop();
console.log(course);

course.instructors.push("Mr.McGee");
console.log(course.instructors);

isMrJohnson = course.instructors.includes("Mr.Johnson");
console.log(isMrJohnson);

function addNewInstructor(instructorName){

	if(course.instructors.includes(instructorName)){
		console.log("Instructor already added.");
	} else {
		course.instructors.push(instructorName)
		console.log("Thank you. Instructor Added.");
	}
}
addNewInstructor("Mr Marco");
addNewInstructor("Mr Marco");
addNewInstructor("Mr Smith");
console.log(course.instructors);

// we can also create/initialize an empty object and then add its properties afterwards

let instructor = {}
console.log(instructor);

instructor.name = "James Johnson";
console.log(instructor);

instructor.age = 56;
instructor.gender = "Male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.class = ["Philosophy","Humanities","Logic"];

console.log(instructor);

instructor.address = {
	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}

console.log(instructor)
console.log(instructor.address.street);

// Create Object Using a Constructor Function

// Create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves as a blueprint for an object

function Superhero(name,superpower,powerLevel){

	// "this" keyword when added in a constructor function refers to the object that will be made by the function

	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;

}

// create an object our of our superhero constructor function


// "new" keyword is added to allow us to create a new object out of our function
let superhero1 = new Superhero("Saitama","One Punch",30000);
console.log(superhero1);

function Laptop(name,brand,price){

	this.name = name;
	this.brand = brand;
	this.price = price;

}

let laptop1 = new Laptop("ASUS TUF Gaming F15","ASUS",74995);

let laptop2 = new Laptop("Macbook Pro 14","Apple","116990");

console.log(laptop1);
console.log(laptop2);

// Object Methods
// Object Methods are functions that are associated with object.
// A function is a property of an object and the functions belongs to the object.
// Methods are tasks that an object can perform/do

let person = {
	name: "Slim Shady",
	// methods are functions associated as a property of an object.
	// THey are anonymous functions we can invoke using the property of the object
	// "this" refers to the object where the method is associated
	talk: function(){
		console.log("Hi! My name is, What? My name is who? " + this.name);
	}
}

let person2 = {
	name: "Dr Dre",
	greet: function(friend){
	// console.log(friend);
	console.log("Good day, " + friend.name);
	}
}

person.talk();
person2.greet(person);

function Dog(name,breed){
	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}
}

let dog1 = new Dog("Rocky","Bulldog");
console.log(dog1);
dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog("Blackie","Rottweiler");
console.log(dog2);
