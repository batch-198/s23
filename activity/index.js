console.log("Welcome to Pallet town!");

let pokeTrainee = {

	name:"Gary Oak",
	age:"12",
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoennRegion: ["Mae","Zinnia"],
		sinnohRegion: ["Barry","Tobias"],
	},
	talk: function(){
		console.log("Pikachu, I choose you!");
	}
}

console.log(pokeTrainee);
console.log("Result of dot notation:");
console.log(pokeTrainee.name);
console.log("Result of square bracket notation:");
console.log(pokeTrainee.pokemon);
console.log("Result of talk function");
pokeTrainee.talk();

	function Pokemon(name,level){

		this.name = name;
		this.level = level;
		this.health = level * 3;
		this.attack = level * 1.5;
		this.tackle = function(enemy){
			console.log(this.name + " tackled " + enemy.name);
			console.log(enemy.name + "'s health reduced to " + (enemy.health - this.attack));
			enemy.health = enemy.health - this.attack;
			console.log(enemy);
		}
		this.faint = function(){
			console.log(this.name + " has fainted!");
		}
		// this.sharpen = function(){
		// 	console.log(this.name + "'s attack increased");
		// 	this.attack = this.attack + 10;
		// 	console.log(this);
		// }
	}

	let pokemon1 = new Pokemon("Pikachu",12);
	console.log(pokemon1);

	let pokemon2 = new Pokemon("Geodude",8);
	console.log(pokemon2);

	let pokemon3 = new Pokemon("MewTwo",100);
	console.log(pokemon3);

	// Attack 1
	pokemon2.tackle(pokemon1);

	// Attack 2
	pokemon3.tackle(pokemon2);
	pokemon2.faint();

	// Trial Attack - Sharpen
	// pokemon1.sharpen();
